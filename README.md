## Bubble Sort
Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.  
Example:  
First Pass:  
( 5 1 4 2 8 ) –> ( 1 5 4 2 8 ), Here, algorithm compares the first two elements, and swaps since 5 > 1.  
( 1 5 4 2 8 ) –>  ( 1 4 5 2 8 ), Swap since 5 > 4  
( 1 4 5 2 8 ) –>  ( 1 4 2 5 8 ), Swap since 5 > 2   
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 ), Now, since these elements are already in order (8 > 5), algorithm does not swap them.
Second Pass:  
( 1 4 2 5 8 ) –> ( 1 4 2 5 8 )  
( 1 4 2 5 8 ) –> ( 1 2 4 5 8 ), Swap since 4 > 2  
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )  
( 1 2 4 5 8 ) –>  ( 1 2 4 5 8 )  
Now, the array is already sorted, but our algorithm does not know if it is completed.  
The algorithm needs one whole pass without any swap to know it is sorted.  
Third Pass:  
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )   
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )  
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )  
( 1 2 4 5 8 ) –> ( 1 2 4 5 8 )  

## Selection Sort
The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending   order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.  

The subarray which is already sorted.  
Remaining subarray which is unsorted.  
  
arr[] = 64 25 12 22 11  
// Find the minimum element in arr[0...4]  
// and place it at beginning  
11 25 12 22 64  
// Find the minimum element in arr[1...4]  
// and place it at beginning of arr[1...4]  
11 12 25 22 64  
// Find the minimum element in arr[2...4]  
// and place it at beginning of arr[2...4]  
11 12 22 25 64  
// Find the minimum element in arr[3...4]  
// and place it at beginning of arr[3...4]  
11 12 22 25 64  

## To use this package  
##### a: npm i sorting-algorithms-rajan  
##### b: const sort=require('sorting-algorithms-rajan');   
##### c: const result=sort.bubbleSort(arrayData)  //for sorting the array data using bubble sort  
##### const result=sort.selectionSort(arrayData)  //for sorting the array data using selection sort  
#####  where arrayData=array;    
   
## Example  
const sort = require('sorting-algorithms-rajan');  
const data = [2, 3, 4, 5, 4, 3, 3, 3, 223131, 132, 32, 132, 232, 3, 23, 23, 23, 23, 45, 23];  
const result = sort.bubbleSort(data);  
console.log(result);  
