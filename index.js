//for the bubbleSort
module.exports.bubbleSort = (data) => {
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length - 1 - i; j++) {
      if (data[j] > data[j + 1]) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
}

//for the selection Sort
module.exports.selectionSort = (data) => {
  for (let i = 0; i < data.length; i++) {
    for (let j = i + 1; j < data.length; j++) {
      if (data[i] > data[j]) {
        temp = data[j];
        data[j] = data[i];
        data[i] = temp;
      }
    }
  }
  return data;
}
